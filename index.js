const mc = require('minecraft-protocol');
const fs = require('fs');
const prompts = require('prompts');

prompts([{
    type: 'text',
    name: 'addr',
    message: 'Address:'
},{
    type: 'text',
    name: 'email',
    message: 'Email:'
},{
    type: 'password',
    name: 'pass',
    message: 'Password:'
}]).then(res=>{
    const credentials = {
        host: res.addr,
        password: res.pass,
        username: res.email,
        auth: 'microsoft'
    };

    if (!fs.existsSync('maps')){
        fs.mkdirSync('maps');
    }

    const client = mc.createClient(credentials);

    client.on('error', (err)=>{
        console.error(err);
    });

    client.on('connect', ()=>{
        console.log('Connected as', client.username);
    });

    client.on('chat', (packet)=>{
        const jsonMsg = JSON.parse(packet.message);
        console.log(jsonMsg);
    });

    let skipmaps = [];
    if(fs.existsSync('skip.txt')){
        let skipFile = fs.readFileSync('skip.txt', 'utf-8');
        skipmaps = skipFile.split(/\r?\n/);
    }

    const maps = [];

    async function noMoreMaps(){
        console.log("Saving maps...");
        client.end();
        await fs.promises.writeFile('maps.json', JSON.stringify(maps, null, 4));
        console.log("Finished saving maps, exiting.");
    }

    let mapTimeout;

    client.on('packet', (data, meta) => {
        if(meta.name != 'map') return;
        if(data.data == undefined) return;
        if(skipmaps.includes(String(data.itemDamage))) return;
        if(maps.includes(data.itemDamage)) return;
        console.log("Receiving map", data.itemDamage);
        maps.push(data.itemDamage);
        fs.writeFile(`maps/${data.itemDamage}.bin`, data.data, function(err){
            if(err) console.error(err);
        });
        if(mapTimeout) clearTimeout(mapTimeout);
        mapTimeout = setTimeout(noMoreMaps, 5000)
    });
});
