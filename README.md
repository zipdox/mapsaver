# Map Saver
Logs into a Minecraft server and downloads all maps within loading distance (in item frames).

# Installation
Make sure NodeJS and NPM are installed and run `npm install`.

# Usage
Run `npm start` and enter the server address and credentials. Afer you stop receiving maps for 5 second the script will exit automatically. A list of maps will be in `maps.json` and the map images will be saved in the maps folder.
